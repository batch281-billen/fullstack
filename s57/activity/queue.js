let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    return collection;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    // In here you are going to remove the first element in the array
    let firstElement = collection[0];
    for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
    }
    collection.length--;
    return firstElement;
}

function front() {
    // you will get the first element
    if (!isEmpty()) {
        return collection[0];
    }
}

// Do not use .length property here

function size() {
     // Number of elements 
    let count = 0;
    for (let _ of collection) {
        count++;
    }
    return count;
}

function isEmpty() {
    //it will check whether the function is empty or not
    return collection[0] === undefined;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};