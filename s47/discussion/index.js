// '.document' refers to the whole web page.
// We are putting it inside a const inline with DRY (Don't Repeat Yourself) method
const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

// Adding the Event Listeners
// innerHTML is used to access the spanFullname
txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})

/*
const updateName = () => {

}

txtFirstName.addEventListener('keyup', updateName)
*/

