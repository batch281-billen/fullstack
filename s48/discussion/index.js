let posts = [];
let count = 1;

// Add post data
// (e) - stands for event
// e.preventDefault - this will prevent the event to be deleted?
document.querySelector("#form-add-post").addEventListener('submit', (e) => {e.preventDefault();

	posts.push({
		id : count,
		title : document.querySelector('#txt-title').value, //kinukuha yung value na ininput ng user sa #txt-title value based from index.html
		body : document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Successfully Added');
});

// Show Posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries; //pinapasok yung postEntries to #div-post-entries at index.html
}

// Edit post
const editPost = (id) => {

	//${id} = no. count nung post (see above)

	let title = document.querySelector(`#post-title-${id}`).innerHTML; 
	//innerHTML - we're accessing/changing an HTML element (inside #post-title-${id})

	let body = document.querySelector(`#post-body-${id}`).innerHTML; 
	//innerHTML - we're accessing/changing an HTML element (inside #post-body-${id})

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}


// Update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	// for loop will iterate 
	for(let i = 0; i < posts.length; i++) {

		//if the current post matches the id we inputted at #txt-edit-id then saka sya mag edit/update
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated.')

			break;
		}
	}

})


// S48 ACTIVITY : DELETE POST
const deletePost = (id) => {
	const index = posts.findIndex((post) => post.id === parseInt(id));

	if (index !== -1) {
		posts.splice(index, 1);
		showPosts(posts);

		const postElement = document.querySelector(`#post-${id}`);

		if (postElement) {
			postElement.remove();
		}

		alert('Successfully deleted.');
	}
}

/* SIR TRISTAN SOLUTION

const deletePost = (id) => {
    posts = posts.filter((post) => {
        if (post.id.toString() !== id) {
            return post;
        }
    });

    document.querySelector(`#post-${id}`).remove();
}


*/