import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard'; --> dinelete din yung CourseCard below under Highlights since nilipat sya kay .pages/Courses.js na

export default function Home(){
	return (
		<Fragment>
			<Banner />
			<Highlights />
		</Fragment>
	)
}

