/*// S52 Activity
import { Button, Form, Row, Col } from 'react-bootstrap';

import { useState, useEffect } from 'react';

export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// this is for the enabling the login/submit button when all the input fields are filled
	const [isDisabled, setIsDisabled] = useState(false);

	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	}, [email, password]);

	// Function to simulate success user login
	function loginUser(event) {

		event.preventDefault();

		alert('You are now logged in!');

		// To return to empty input fields upon successful login
		setEmail('');
		setPassword('');
	}

	return (
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Login</h1>
				<Form onSubmit = {event => loginUser(event)}>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email"
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	/>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password}
				        	onChange = {event => setPassword(event.target.value)}
				        	/>
				      </Form.Group>
				      
				      <Button variant="success" type="submit" disabled = {isDisabled}>
				        Login
				      </Button>
				    </Form>
			</Col>
		</Row>
	)
}

*/

// SIR CHRIS S52 SOLUTION

import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';

// sweetalert2 is a simple and useful package for generating user alerts with ReactJS
import Swal2 from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

   	const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    // Allows us to consume the UserContext object and its properties to use for user validation
    const { user, setUser } = useContext(UserContext);

    // const [user, setUser] = useState(localStorage.getItem('email')); - kinomment out kasi gagamit na ng userContext

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // set the email of the authenticated user in the local storage
        // Syntax:
        	// localStorage.setItem('property', value);

        // The localStorage.setItem() allows is to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering.
        // Because REACT JS is a single page application, using the localStorage will not trigger re-rendering (or reload) of component.
        // localStorage.setItem('email', email);

        // This monitors the logged in user na kaya once na-authenticate rerekta na to homepage
        // setUser(localStorage.getItem('email'));

        // alert('Login successful!');
        // navigate('/courses');

        // Clear input fields after submission
        // setEmail('');
        // setPassword('');

        // process fetch request to the corresponding backend API
        //Syntax:
            //fetch('url', {options}).then(response => response.json()).then(data => {});

        // environment variable (process.env.REACT_APP_API_URL) The environment variables are important for hiding sensitive pieces of information like the backend API URL which can be exploited if added directly into our code.
        // Para kapag magpapalit ng backend application environment, dun na lang magpapalit sa .env.local. Just change the backend server info (http:localhost:4000)
        // EVERY TIME na magCHANGE ng sa .env.local, need to restart yung react app

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
                    method: 'POST',
                    headers: {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password
                    })
                })
        .then(response => response.json())
        .then(data => {
            // console.log(data);
            // localStorage.getItem('token', data);

            //if statement to check whether the login is successful.
            if(data === false){
                // alert('Login unsuccessful!');
                // In adding sweetalert2 you have to use the fire method
                Swal2.fire({
                	title: 'Login unsuccessful',
                	icon: 'error',
                	text: 'Check your login credentials and try again'
                })
            
            }else{
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                // alert('Login successful!');

                Swal2.fire({
                	title: 'Login successful!',
                	icon: 'success',
                	text: 'Welcome to Zuitt!'
                })

                navigate('/courses');
            }

        })
    }

    const retrieveUserDetails = (token) => {

    	fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    		headers: {
    			Authorization: `Bearer ${token}`
    		}
    	})
    	.then(response => response.json())
    	.then(data => {
    		setUser({
    			id: data._id,
    			isAdmin: data.isAdmin
    		})
    	})
    }


    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== '' && email.length<=15){
            setIsActive(false);
        }else{
            setIsActive(true);
        }

    }, [email, password]);

//If the user is not yet logged in, login page is shown. But, if already logged in, it will be redirected to home page (need to refresh).
    return (
        user.id === null || user.id === undefined
        ?
        	<Row>
        	    <Col className = 'col-6 mx-auto'>
        	        <h1 className = 'text-center mt-2'>Login</h1>
        	        <Form onSubmit={(e) => authenticate(e)}>
        	            <Form.Group controlId="userEmail">
        	                <Form.Label>Email address</Form.Label>
        	                <Form.Control 
        	                    type="email" 
        	                    placeholder="Enter email"
        	                    value={email}
        	                    onChange={(e) => setEmail(e.target.value)}
        	                    required
        	                />
        	            </Form.Group>

        	            <Form.Group controlId="password">
        	                <Form.Label>Password</Form.Label>
        	                <Form.Control 
        	                    type="password" 
        	                    placeholder="Password"
        	                    value={password}
        	                    onChange={(e) => setPassword(e.target.value)}
        	                    required
        	                />
        	            </Form.Group>
        	            
        	            <Button variant="primary" type="submit" id="submitBtn" disabled = {isActive} className ='mt-3'>
        	                    Login
        	                </Button>
        	            
        	        </Form>
        	    </Col>
        	</Row>

        :
            //navigating to PageNotFound once binago yung endpoint to /login
            <Navigate to ='/*' />
    )
}
