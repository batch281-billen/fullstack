// It will allows us to navigate from one page of our application to another page
import { Navigate } from 'react-router-dom';

import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	// It will allow us to clear all the information in the localStorage ensuring no information is stored in our browser
	// localStorage.clear(); - comment out since included na sya kay unsetUser

	const { unsetUser, setUser } = useContext(UserContext);

	useEffect(() => {
		// The value from our localStorage will be cleared
		unsetUser();
		// Since we cleared the contents of our local storage using the unsetUser() therefore the value of our user state will be null.
		// By adding useEffect, this will allow the Logout page to render first before triggering the useEffect which changes the state of our user.
		// setUser(localStorage.getItem('email'));
		// console.log(user); - dapat included si user sa const {unsetUser, setUser, user} = useContext(UserContext) if gamitin itong console.log

		setUser({
			id: null,
			isAdmin: null
		})

	}, [])

	

	return (
		<Navigate to = '/login'/>
	)
}