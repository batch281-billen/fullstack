// Inimport yung below code from forms-react bootstrap website
// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

// shorthand code for importing button and form
import { Button, Form, Row, Col } from 'react-bootstrap';

// We need to import the useState from the react
import { useState, useEffect, useContext } from 'react';

import Swal2 from 'sweetalert2';

import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
// import PageNotFound from './PageNotFound';

import Login from './Login';

export default function Register(){
	
	// State hooks to store the values of the input fields
	// We need to create 3 states kasi 3 input fields yung need ng input

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	//this is for the set email length condition <= 15
	// const [isPassed, setIsPassed] = useState(false);

	const [isPassed, setIsPassed] = useState(true);

	// this is for the enabling the login/submit button when all the input fields are filled
	const [isDisabled, setIsDisabled] = useState(true);

	// We are going to add/create a state that will declare whether the password1 and password2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true);


	// When the email changes, it will have a side effect that will console its value
	useEffect(() => {
		if(email.length > 15){
			setIsPassed(false);
		} else {
			setIsPassed(true);
		}
	}, [email]);

	// This useEffect will disable or enable our sign up button if all the set conditions below are met
	useEffect(() => {
		// We are going to add "if" statement and all the condition that we mention should be satisfied before we enable the sign up button
		// If the email, password1, & password2 is not equal to empty string. Then, if password1 & password2 is strictly equal. Then if the email length of characters is less than or equal to 15.
		if(firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && mobileNo.length !== 11 && password1 !== '' && password2 !== '' && password1 === password2 && email.length <= 15) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	}, [firstName, lastName, email, mobileNo, password1, password2]);



	// Function to simulate user registration
	function registerUser(event) {
		// Prevent page reloading
		event.preventDefault();

		// alert('Thank you for registering!');

		// navigate('/login');

		// To return to empty input fields upon successful registration
		// setEmail('');
		// setPassword1('');
		// setPassword2('');

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then((data) => {
			if (data) {
				Swal2.fire({
					title: 'Duplicate email found!',
					icon: 'info',
					text: 'Email already exists.'
				});
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				.then(response => response.json())
				.then((data) => {
					if (data.email) {
						Swal2.fire({
							title: 'Registration unsuccessful!',
							icon: 'error',
							text: 'Check your registration details and register again.'
						});
						
						
					} else {
						Swal2.fire({
							title: 'Registration Successful!',
							icon: 'success',
							text: 'Welcome to Zuitt!'
						})
						navigate('/login');
					}
				});
			}
		});
	}

		


	useEffect(() => {
		if(mobileNo.length !== 11) {
			setIsPassed(false);
		} else {
			setIsPassed(true);
		}
	}, [mobileNo]);


	//useEffect to validate whether the password1 is equal to password2

	/*useEffect(() => {
		if (password1 === password2) {
			setIsPasswordMatch(true);
		} else {
			setIsPasswordMatch(false);
		}

	}, [password1, password2]);*/

	useEffect(() => {
		if (password1 !== password2) {
			setIsPasswordMatch(false);
		} else {
			setIsPasswordMatch(true);
		}

	}, [password1, password2]);

	return (
		user.id === null || user.id === undefined
		?
			<Row>
				<Col className = "col-6 mx-auto">
					<h1 className = "text-center">Register</h1>
					<Form onSubmit = {event => registerUser(event)}>
					      
					      <Form.Group className="mb-3" controlId="firstName">
					        <Form.Label>First Name</Form.Label>
					        <Form.Control 
					        	type="text" 
					        	placeholder="Enter First Name"
					        	value = {firstName}
					        	required
					        	onChange = {event => setFirstName(event.target.value)}
					        	/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="lastName">
					        <Form.Label>Last Name</Form.Label>
					        <Form.Control 
					        	type="text" 
					        	placeholder="Enter Last Name"
					        	value = {lastName}
					        	required
					        	onChange = {event => setLastName(event.target.value)}
					        	/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="userEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control 
					        	type="email" 
					        	placeholder="Enter email"
					        	value = {email}
					        	required
					        	onChange = {event => setEmail(event.target.value)}
					        	/>
					        <Form.Text className="text-danger" hidden = {isPassed}>
					          The email should not exceed 15 characters.
					        </Form.Text>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="mobileNo">
					        <Form.Label>Mobile Number</Form.Label>
					        <Form.Control 
					        	type="text" 
					        	placeholder="Enter Mobile Number"
					        	value = {mobileNo}
					        	required
					        	onChange = {event => setMobileNo(event.target.value)}
					        	/>
					        <Form.Text className="text-danger" hidden = {isPassed}>
					          The mobile number should not exceed 11 digits.
					        </Form.Text>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="password1">
					        <Form.Label>Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	placeholder="Password"
					        	value={password1}
					        	required
					        	onChange={event => setPassword1(event.target.value)}
					        	/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="password2">
					        <Form.Label>Confirm Password</Form.Label>
					        <Form.Control 
					        	type="password" 
					        	placeholder="Retype your nominated password"
					        	value={password2}
					        	required
					        	onChange={event => setPassword2(event.target.value)} 
					        	/>
					      </Form.Group>

					      <Form.Text className="text-danger" hidden = {isPasswordMatch}>
					        The passwords does not match!
					      </Form.Text>

					      
					      <Button variant="primary" type="submit" disabled = {isDisabled}>
					        Sign up
					      </Button>
					    </Form>
				</Col>	
			</Row>
		:
			<Login />
	)
}