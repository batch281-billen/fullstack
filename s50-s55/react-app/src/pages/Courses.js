import { Fragment, useState, useEffect } from 'react';
import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses() {
	//Check to see if the mock data was captured
	// console.log(coursesData);

	const [courses, setCourses] = useState([]);

	// We are going to add a useEffect here so that every time that we refresh our application, it will fetch the updated content of our courses.
	// Ang i-fetch dito e yung laman ng courses
	// No need na maglagay ng method since automatic na GET method kapag no indicated method

	// nirefactor na ito based sa database MongoDB. instead of creating a new variable, nagcreata na lang above ng state to contain yung data. 
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	}, [])

	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp

	/*const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)
	})*/

	//The course in the CourseCard component is called a 'prop' which is a shorthand for "property" since components are considered as objects in ReactJS

	// The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ""

	// We can pass information from one component to another using props. This is referred to as props drilling.

	/*
	return (
		<Fragment>
			<CourseCard courseProp={coursesData[0]}/>
		</Fragment>
	)
	ginawa na lang courses below since nag const ng courses from above code then .map method na para magloop through the data courses dun sa array
	*/

	return (
		<Fragment>
			{courses}
		</Fragment>
	)
}