//si App.js yung nagrerender then need imount here yung container para makita sa website
// import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import AppNavBar from './components/AppNavBar';

// kinomment out na kasi kay Home na sya papasok. Naka separate of concerns na si Home
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';

import Courses from './pages/Courses';
import Home from './pages/Home';

//Need comment out si Register page para si Login page yung lalabas in line with s52 activity

import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';

import { useState, useEffect } from 'react';

import CourseView from './components/CourseView';

// Import the UserProvider from the UserContext to provide the content or value of our UserContext
import { UserProvider } from './UserContext';

import './App.css';

// import necessary modules from react-router-dom
import {BrowserRouter, Route, Routes} from 'react-router-dom';


// React JS is a SPA
// Whenever a link is clicked, it functions as if the page is being reloaded, but what actually happens is it goes through the process of rendering, mounting, re-rendering and remounting.


// The 'BrowserRouter' component will enable us to simulate page navigation by synchronizing the show content and the shown URL in the web browser

// 'Routes' component holds all our Route component. It selects which 'Route' component to how based on the URL endpoint. For example, when the '/courses' is visited in the web browser React.js will show the Courses component to us.

// Here yung nirerender sa React.StrictMode sa index.js
function App() {
  // Create a State hook for the user state that defines here for global scoping
  // Initialized as the value of the user information from the browser's localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  // const [user, setUser] = useState(localStorage.getItem('email'));

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });


  // This Function is for clearing the localStorage on logout
  const unsetUser = () => {
    localStorage.clear()
  };


  // useEffect(() => {
  //   console.log(user)
  // }, [user]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(data => {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    })
  }, [])

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
         <AppNavBar />
          <Container>
            <Routes>
              <Route path ='/' element = {<Home/>}/>
              <Route path = '/courses' element = {<Courses/>}/>
              <Route path ='/register' element = {<Register/>}/>
              <Route path = '/login' element = {<Login/>} />
              <Route path = '/logout' element = {<Logout/>} />
              <Route path = '/courses/:id' element = {<CourseView/>} />
              <Route path = '*' element = {<PageNotFound/>} />
            </Routes>
         </Container>
      </BrowserRouter>
    </UserProvider>
  );
}


export default App;
