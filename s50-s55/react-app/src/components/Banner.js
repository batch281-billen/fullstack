/*
import Button from 'react-bootstrap/Button';
import { Row } from 'react-bootstrap/Row';
import { Col } from 'react-bootstrap/Col';
*/

// Pinasok na lang yung above codes sa isang line (below code) for Button, Row, & Column
import { Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Banner(){
	return (
		<Row>
            <Col className = "p-5 offset-auto" >
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button as = {Link} to = '/courses' variant="primary">Enroll now!</Button>
            </Col>
        </Row>
	)
}