import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { useState, useEffect, useContext } from 'react';
import { NavLink, Link } from 'react-router-dom';

import UserContext from '../UserContext';

// NavLink - appropriate use to traverse on different routes 
// Link - appropriate to use within the page

// The 'as' keyword allows components to be treated as if they are different component gaining access to it's properties and functionalities

// By using the 'as' keyword, yung functionality ni NavLink e inabsorb ni Nav.Link coming from the react-router-dom

// The 'to' keyword is used in place of the 'href' for providing the URL for the page



export default function AppNavBar() {

    // Ang kinonsume lang from useContext from UserContext from react is si user
    // Eto yung pag nag log in, automatic na magbabago si AppNavBar wherein si Logout na yung mag appear since connected na or nag uusap na globally si App.js, Login, AppNavBar with UserContext.
    const { user } = useContext(UserContext);

    // console.log(user); - to show lang yung null value once mag log out and setUser na
    // In here, I will capture the value from our localStorage and then store it in a state.
    // Syntax:
        // localStorage.getItem('key/property')
    
    // console.log(localStorage.getItem('email')); 
    
    // const [user, setUser] = useState(localStorage.getItem('email')); - kinomment out due to using of UserContext


    /*useEffect(() => {

        setUser(localStorage.getItem('email'))

    }, [localStorage.getItem('email')])*/


    return (
        <Navbar bg="primary" expand="lg">
            <Container fluid>
                <Navbar.Brand as = {Link} to = '/'>Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                    <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
                    <Nav.Link as = {NavLink} to = '/courses'>Courses</Nav.Link>
                    
                    {
                        user.id === null || user.id === undefined
                        ?
                        <>
                        <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
                        <Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
                        </>
                        :
                        <Nav.Link as = {NavLink} to = '/logout'>Logout</Nav.Link>
                    }
            
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>

    )
}



